from pathlib import Path
from concho.dell import DellMatinfoCsvConfigurator
from concho.dell import MatinfoConfigurator
from concho.dell import DellConfiguratorParser2020
from concho.dell import DellConfiguratorParser2021
from concho.hpe import HpeConfiguratorParser, HpeCpuChoiceConfiguratorParser
from concho.procs_chooser import plot_configurators
from concho.procs_chooser import ConfigPrice
# from concho.procs_chooser import ConfigFlops
from concho.procs_chooser import ConfigFlopsPerEuro


def test_all_matinfo_2020_configs():
    # configurator = DellMatinfoConfigurator('rcrc1406676-4834664 - Cat2 Conf4 PowerEdge R640 - Dell.html')
    # print(configurator)
    configurators = [
        DellMatinfoCsvConfigurator(Path('c6420-20200716-price.tsv')),
        MatinfoConfigurator(Path('rcrc1406676-4834664 - Cat2 Conf4 PowerEdge R640 - Dell.html'), DellConfiguratorParser2020()),
        MatinfoConfigurator(Path('rcrc1406676-4824727 - Cat 2 Conf 7 PowerEdge R940 - Dell.html'), DellConfiguratorParser2020()),
        # dell.DellPowerEdgeR940(),
    ]

    plot_configurators(configurators=configurators, ram_per_core=4.0e9, xaxis_def=ConfigPrice(), yaxis_def=ConfigFlopsPerEuro(), plot_title='total cost including electricity')


def test_credits_2020_configs():
    # configurator = DellMatinfoConfigurator('rcrc1406676-4834664 - Cat2 Conf4 PowerEdge R640 - Dell.html')
    # print(configurator)
    configurators = [
        DellMatinfoCsvConfigurator(Path('c6420-20200716-price.tsv')),
        MatinfoConfigurator(Path('rcrc1406676-4834664 - Cat2 Conf4 PowerEdge R640 - Dell.html'), DellConfiguratorParser2020()),
        MatinfoConfigurator(Path('rcrc1406676-4824727 - Cat 2 Conf 7 PowerEdge R940 - Dell.html'), DellConfiguratorParser2020()),
        # dell.DellPowerEdgeR940(),
    ]

    # config_filter = lambda config : config.cpu.uid in [
    #     'intel-xeon-gold-5222',
    #     'intel-xeon-gold-6226r',
    #     'intel-xeon-gold-6230r',
    #     'intel-xeon-gold-6234r',
    #     'intel-xeon-gold-6240r',
    #     'intel-xeon-gold-6248r',
    #     'intel-xeon-gold-6230',
    #     'intel-xeon-gold-6240',
    #     ]

    def config_filter(config):
        return config.get_price() < 40000.0

    plot_configurators(configurators=configurators, ram_per_core=4.0e9, xaxis_def=ConfigPrice(), yaxis_def=ConfigFlopsPerEuro(), plot_title='physmol/ts credit 2020 configs', config_filter=config_filter)


def test_credits_2021_configs():
    configurators = [
        MatinfoConfigurator(Path('20210407 - Cat2 Conf4 PowerEdge R640 - Dell.html'), DellConfiguratorParser2021()),
        MatinfoConfigurator(Path('20210407 - Cat2 Conf7 PowerEdge R940 - Dell.html'), DellConfiguratorParser2021()),
        MatinfoConfigurator(Path('20210407 - Cat2 Conf8 PowerEdge R7525 - Dell.html'), DellConfiguratorParser2021()),
        # MatinfoConfigurator('20210407 - Cat2 Conf10 PowerEdge R6525 - Dell.html', DellConfiguratorParser2021()),
    ]
    # config_filter = lambda config : config.cpu.uid in [
    #     'intel-xeon-gold-5222',
    #     'intel-xeon-gold-6226r',
    #     'intel-xeon-gold-6230r',
    #     'intel-xeon-gold-6234r',
    #     'intel-xeon-gold-6240r',
    #     'intel-xeon-gold-6248r',
    #     'intel-xeon-gold-6230',
    #     'intel-xeon-gold-6240',
    #     ]

    def config_filter(config):
        return config.get_price() < 40000.0

    plot_configurators(configurators=configurators, ram_per_core=4.0e9, xaxis_def=ConfigPrice(), yaxis_def=ConfigFlopsPerEuro(), plot_title='physmol/ts credit 2021 configs', config_filter=config_filter)


def test_ur1_presents_2023_configs():
    configurators = [
        # MatinfoConfigurator('20210407 - Cat2 Conf4 PowerEdge R640 - Dell.html', DellConfiguratorParser2021()),
        MatinfoConfigurator(Path('20230120-cat2-conf3-hpe-dl360-gen10.html'), HpeConfiguratorParser()),
        MatinfoConfigurator(Path('20230123-cat2-conf10-hpe-dl360-gen10plus-cpuchoice.html'), HpeCpuChoiceConfiguratorParser()),
        MatinfoConfigurator(Path('20230123-cat2-conf11-hpe-dl385-gen10plus-cpuchoice.html'), HpeCpuChoiceConfiguratorParser()),
    ]

    def config_filter(config):
        return True  # config.get_price() < 40000.0

    plot_configurators(configurators=configurators, ram_per_core=4.0e9, xaxis_def=ConfigPrice(), yaxis_def=ConfigFlopsPerEuro(), plot_title='physmol/ts credit 2023 configs', config_filter=config_filter)
    # plot_configurators(configurators=configurators, ram_per_core=4.0e9, xaxis_def=ConfigPrice(), yaxis_def=ConfigFlops(), plot_title='physmol/ts credit 2023 configs', config_filter=config_filter)


if __name__ == '__main__':
    test_ur1_presents_2023_configs()
