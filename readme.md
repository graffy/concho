# ConCho : Configuration Choser

a tool to help choosing the best quality price ratio for a compute node

usage:
```console
graffy@graffy-ws2:~/work/concho$ PYTHONPATH=. python3 ./tests/test1.py 
```


# intel dynamic frequency scaling

## xeon silver 4116

[https://en.wikichip.org/wiki/intel/xeon_silver/4116#Frequencies]

Turbo Frequency/Active Cores
| Mode   | Base      |         1 |         2 |         3 |         4 |         5 |         6 |         7 |         8 |         9 |        10 |        11 |        12 |
| :----- | :-------- | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: |
| Normal | 2,100 MHz | 3,000 MHz | 3,000 MHz | 2,800 MHz | 2,800 MHz | 2,700 MHz | 2,700 MHz | 2,700 MHz | 2,700 MHz | 2,400 MHz | 2,400 MHz | 2,400 MHz | 2,400 MHz |
| AVX2   | 1,700 MHz | 2,900 MHz | 2,900 MHz | 2,700 MHz | 2,700 MHz | 2,400 MHz | 2,400 MHz | 2,400 MHz | 2,400 MHz | 2,100 MHz | 2,100 MHz | 2,100 MHz | 2,100 MHz |
| AVX512 | 1,100 MHz | 1,800 MHz | 1,800 MHz | 1,600 MHz | 1,600 MHz | 1,500 MHz | 1,500 MHz | 1,500 MHz | 1,500 MHz | 1,400 MHz | 1,400 MHz | 1,400 MHz | 1,400 MHz |


## xeon gold 6140

[https://en.wikichip.org/wiki/intel/xeon_gold/6140]
Turbo Frequency/Active Cores

| Mode   | Base      |         1 |         2 |         3 |         4 |         5 |         6 |         7 |         8 |         9 |        10 |        11 |        12 |        13 |        14 |        15 |        16 |        17 |        18 |
| :----- | :-------- | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: | --------: |
| Normal | 2,300 MHz | 3,700 MHz | 3,700 MHz | 3,500 MHz | 3,500 MHz | 3,400 MHz | 3,400 MHz | 3,400 MHz | 3,400 MHz | 3,400 MHz | 3,400 MHz | 3,400 MHz | 3,400 MHz | 3,100 MHz | 3,100 MHz | 3,100 MHz | 3,100 MHz | 3,000 MHz | 3,000 MHz |
| AVX2   | 1,900 MHz | 3,600 MHz | 3,600 MHz | 3,400 MHz | 3,400 MHz | 3,300 MHz | 3,300 MHz | 3,300 MHz | 3,300 MHz | 3,000 MHz | 3,000 MHz | 3,000 MHz | 3,000 MHz | 2,700 MHz | 2,700 MHz | 2,700 MHz | 2,700 MHz | 2,600 MHz | 2,600 MHz |
| AVX512 | 1,500 MHz | 3,500 MHz | 3,500 MHz | 3,300 MHz | 3,300 MHz | 2,800 MHz | 2,800 MHz | 2,800 MHz | 2,800 MHz | 2,400 MHz | 2,400 MHz | 2,400 MHz | 2,400 MHz | 2,100 MHz | 2,100 MHz | 2,100 MHz | 2,100 MHz | 2,100 MHz | 2,100 MHz |

## xeon gold 6240

[https://en.wikichip.org/wiki/intel/xeon_gold/6240]
Turbo Frequency/Active Cores

| Mode   | Base     |        1 |        2 |        3 |        4 |        5 |        6 |        7 |        8 |        9 |       10 |       11 |       12 |       13 |       14 |       15 |       16 |       17 |       18 |
| :----- | :------- | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: |
| Normal | 2,600 MHz  | 3,900 MHz  | 3,900 MHz  | 3,700 MHz  | 3,700 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,300 MHz  | 3,300 MHz  |
| AVX2   | 2,000 MHz  | 3,700 MHz  | 3,700 MHz  | 3,500 MHz  | 3,500 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,200 MHz  | 3,200 MHz  | 3,200 MHz  | 3,200 MHz  | 2,900 MHz  | 2,900 MHz  | 2,900 MHz  | 2,900 MHz  | 2,800 MHz  | 2,800 MHz  |
| AVX512 | 1,600 MHz  | 3,700 MHz  | 3,700 MHz  | 3,500 MHz  | 3,500 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 2,900 MHz  | 2,900 MHz  | 2,900 MHz  | 2,900 MHz  | 2,600 MHz  | 2,600 MHz  | 2,600 MHz  | 2,600 MHz  | 2,500 MHz  | 2,400 MHz  |

## xeon gold 6248

[https://en.wikichip.org/wiki/intel/xeon_gold/6248]
Turbo Frequency/Active Cores

| Mode   | Base     |        1 |        2 |        3 |        4 |        5 |        6 |        7 |        8 |        9 |       10 |       11 |       12 |       13 |       14 |       15 |       16 |       17 |       18 |       19 |       20 |
| :----- | :------- | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: | -------: |
| Normal | 2,500 MHz  | 3,900 MHz  | 3,900 MHz  | 3,700 MHz  | 3,700 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,600 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,200 MHz  | 3,200 MHz  | 3,200 MHz  | 3,200 MHz  |
| AVX2   | 1,900 MHz  | 3,800 MHz  | 3,800 MHz  | 3,600 MHz  | 3,600 MHz  | 3,500 MHz  | 3,500 MHz  | 3,500 MHz  | 3,500 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,400 MHz  | 3,000 MHz  | 3,000 MHz  | 3,000 MHz  | 3,000 MHz  | 2,800 MHz  | 2,800 MHz  | 2,800 MHz  | 2,800 MHz  |
| AVX512 | 1,600 MHz  | 3,800 MHz  | 3,800 MHz  | 3,600 MHz  | 3,600 MHz  | 3,500 MHz  | 3,500 MHz  | 3,500 MHz  | 3,500 MHz  | 3,000 MHz  | 3,000 MHz  | 3,000 MHz  | 3,000 MHz  | 2,700 MHz  | 2,700 MHz  | 2,700 MHz  | 2,700 MHz  | 2,500 MHz  | 2,500 MHz  | 2,500 MHz  | 2,500 MHz  |