from setuptools import setup, find_packages


setup(name='concho',
    version='1.0',
    packages=find_packages(),
    install_requires=['lxml', 'numpy', 'matplotlib'])